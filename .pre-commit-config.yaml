---

# To onboard:
#
# 1. install pre-commit app. This can be done either once, globally on your workstation,
#    or locally in your virtual environment (if you have such):
#
#    $ pip install pre-commit
#
# 2. install pre-commit hook in your working copy of a git repository:
#
#    $ pre-commit install
#
# From now on, git will run pre-commit hook before every `git commit`.
#
# See https://pre-commit.com for more information
# See https://pre-commit.com/hooks.html for more hooks

repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks.git
    rev: v4.4.0
    hooks:
      - id: check-added-large-files
      - id: check-merge-conflict
      - id: check-symlinks
      - id: check-yaml
      - id: end-of-file-fixer
      - id: mixed-line-ending
      - id: no-commit-to-branch
        args:
          - "--branch"
          - "main"
      - id: trailing-whitespace

  - repo: https://github.com/Lucas-C/pre-commit-hooks
    rev: v1.4.2
    hooks:
      - id: insert-license
        files: \.py$
        args:
          - --license-filepath
          - LICENSE_SPDX

  - repo: https://github.com/pycqa/isort
    rev: 5.12.0
    hooks:
      - id: isort
        args:
          - "--profile=black"
          - "--filter-files"

  - repo: https://github.com/psf/black.git
    rev: 23.1.0
    hooks:
      - id: black
        language_version: python3
        # workaround https://github.com/psf/black/issues/2964
        additional_dependencies: ['click==8.0.4']
        args:
          - "--config=pyproject.toml"

  - repo: https://github.com/pycqa/flake8.git
    rev: 6.0.0
    hooks:
      - id: flake8
        args:
          - "--config=tox.ini"
        files: >
          (?x)^(
            src\/.*\.py
          )$

  - repo: https://github.com/semgrep/semgrep
    rev: 'v1.68.0'
    hooks:
      - id: semgrep
        args: ['--config', 'p/r2c-ci', '--error']
        files: >
          (?x)^(
            src\/.*\.py
          )$
